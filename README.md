# camel-samples

Talk : **Prépa talk MaxCamp 2022 - Apache Camel, le framework pour devenir un plombier superstar 📝**

## Liens utiles

* Lien vers le ticket du CFP : https://gitlab.com/maxds/maxcamp/-/issues/113

## Live Coding

* Présenter projet quasi vide (pom.xml & @SpringBootApplication)

* Créer une nouvelle classe TimerToLog, étendre RouteBuilder

* Faire le code suivant :
```java 
from("timer:start")
    .log("End of route for ${body}")
```

* Ensuite rajouter :
```java 
from("timer:start")
    .setBody(simple("Coucou"))
    .log("End of route for ${body}")
```

* Ensuite rajouter :
```java 
from("timer:start")
    .routeId("timerToLogRoute")
    .setBody(simple("Coucou"))
    .log("End of route for ${body}")
```

* Ensuite ajouter (découverte de `.process()`):
```java 
from("timer:start")
    .routeId("timerToLogRoute")
    .setBody(simple("Coucou"))
    .process(exchange -> exchange.getIn().setBody("Origin body was " + exchange.getIn().getBody(String.class)))
    .log("End of route for ${body}")
```

* Maintenant on va :
    - créer un POJO Character avec 2 champs `name` et `xp`
    - créer une classe de Mapping prenant un nom en retournant un Character
    - Pour simuler l'entrée on va faire un random sur 3 characters :
```java 
List<String> givenCharacters = Arrays.asList("Mario", "Luigi", "Peach");

from("timer:start")
    .routeId("timerToLogRoute")
    .process(exchange -> {
        Random rand = new Random();
        exchange.getIn().setBody(givenCharacters.get(rand.nextInt(givenCharacters.size())));
    })
    .bean(MappingBean.class)
    .log("End of route for ${body}")
```

* Grace à Lombok le `.toString()` est explicite sur le POJO, mais en Json c'est mieux pour communiquer avec l'extérieur :
    - Ajout dépendance suivante
```xml
<dependency>
    <groupId>org.apache.camel.springboot</groupId>
    <artifactId>camel-jackson-starter</artifactId>
    <version>${camel.version}</version>
</dependency>
```

- Ajout de la ligne :
```java 
    .marshal().json(JsonLibrary.Jackson)
```

* Tiens, regardons maintenant comment Désérialiser un message Json

```java 
from("timer:start")
    .routeId("timerToLogRoute")
    .process(exchange -> {
        Random rand = new Random();
        exchange.getIn().setBody(givenCharacters.get(rand.nextInt(givenCharacters.size())));
    })
    .bean(MappingBean.class)
    .marshal().json(JsonLibrary.Jackson)
    .to("direct:unmarshal")
;

from("direct:unmarshal")
    .routeId("unmarshal")
    .unmarshal().json(JsonLibrary.Jackson, Character.class)
    .log("End of route for ${body}")
;
```

* Rajoutons une méthode au bean de Mapping :
```java
public void gainXp(Exchange exchange) {
    Character character = exchange.getIn().getBody(Character.class);
    character.setXp(character.getXp() + new Random().nextInt(10));
}
```

* l'appli plante, elle ne sait pas quelle méthode doit être appelée, on modifie la signature de l'appel au bean pour ajouter le nom de la méthode -> c'est ok

* on ajoute l'appel à l'autre méthode de la même manière

C'est good :-)

* On va désormais essayer d'envoyer les messages quelque part ... genre Slack
```xml 
<dependency>
    <groupId>org.apache.camel.springboot</groupId>
    <artifactId>camel-slack-starter</artifactId>
    <version>${camel.version}</version>
</dependency>
```

* Dans le bean de Mapping :
```java 
public Message prepareSlackMessage(Character character) {
    Message message = new Message();
    message.setBlocks(Collections.singletonList(SectionBlock
            .builder()
            .text(MarkdownTextObject
                    .builder()
                    .text("*Hello from Camel!* I'm " + character.getName() + " with *" + character.getXp() + " XP*")
                    .build())
            .build()));
    return message;
}
```

* Rajouter dans la route avant le log de fin :
```java
.to("slack:#maxcamp?webhookUrl=https://hooks.slack.com/services/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")
```

Yeah !!!

* Ok maintenant on veut pousser les personnages depuis Slack

```java
from("slack://maxcamp?maxResults=1&serverUrl=https://maxds.slack.com&token=xoxb-54896018115-4181098802850-jcvP1SNU54x15Erzlqzsf9iE")
        .routeId("fromSlack")
        .log("Incoming Slack message received from user ${body.user}")
        .choice()
        .when(simple("${body.text} == 'Mario' || ${body.text} == 'Luigi' || ${body.text} == 'Peach'")).to("direct:processCharacter")
        .otherwise().log("Do Nothing, the message wasn't for us.")
        .end()
;
```

Seuls certains messages nous intéressent `Mario`, `Luigi`, `Peach` :

```java
.choice()
.when(simple("${body.text} == 'Mario' || ${body.text} == 'Luigi' || ${body.text} == 'Peach'")).to("direct:processCharacter")
.otherwise().log("Do Nothing, the message wasn't for us.")
.end()
```

* Il faut code le reste du flux :
```java
from("direct:processCharacter")
        .bean(MappingBean.class, "readFromSlack")
        .marshal().json(JsonLibrary.Jackson)
        .to("direct:unmarshal")
;
```

```java
public Character readFromSlack(Message slackMessage) {
    return new Character(slackMessage.getText(), 0);
}
```

### Bilan

On a vu pendant ce live-coding

* L'intégration ultra légère dans Spring Boot
* Le DSL Java
* Mécanisme de routes Camel, qui peuvent s'appeler entre elles en direct ou via des briques de découplage (broker, fichier etc...)
* La syntaxe des expressions Camel
* Les structures conditionnelles
* L'utilisation de composants sur étagère
* La sérialisation/désérialisation via Jackson
* Les mécanismes d'appel de traitements métier (Beans vs Processor)